package maindatart.metadata;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author 丁丁
 * 
 * 用于在元数据里描述主数据的属性信息。
 * 
 */
public class MainDataPropertyEntity {

	
	public static final String DT_STRING = "string";
	public static final String DT_INT = "int";
	public static final String DT_DOUBLE = "double";
        public static final String DT_LONG = "long";
        public static final String DT_FLOAT = "float";
        public static final String DT_DATE = "date";
        public static final String DT_TIME = "time";
              
        
        public static final List<String> typeList = new ArrayList();
        
        static {
            typeList.add(DT_STRING);
            typeList.add(DT_INT);
            typeList.add(DT_DOUBLE);
            typeList.add(DT_LONG);
            typeList.add(DT_FLOAT);
            typeList.add(DT_DATE);
            typeList.add(DT_TIME);
        }
        
        //ID
	private String mdp_id;
	//对应主数据元数据的ID
	private String mdepid;
	//属性名称
	private String name;
	//是否可以为空
	private boolean nullable;
	//数据类型
	private String datatype;
	//该属性长度
	private int length;
        //排序
	private int position;
        
	public String getMdp_id() {
		return mdp_id;
	}

	public void setMdp_id(String mdp_id) {
		this.mdp_id = mdp_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
        public String getMdepid() {
        return mdepid;
        }

        public void setMdepid(String mdepid) {
            this.mdepid = mdepid;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
        
         //校验函数
        public boolean valedata(){
            return true;
        }
}
