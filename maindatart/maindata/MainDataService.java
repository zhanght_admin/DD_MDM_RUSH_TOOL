/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.maindata;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import maindatart.metadata.MainDataMetaDataEntity;
import maindatart.metadata.MainDataPropertyEntity;
import maindatart.metadata.MetaDataDao;
import maindatart.util.C3p0Source;
import maindatart.util.MainDataSQLBuilder;

/**
 *
 * @author 丁丁
 * 
 * 主数据服务类
 * 
 * 
 */
public class MainDataService {
    
    
    /**
     * 根据主数据描述获取所有主数据记录
     * @param mdmd
     * @return 
     * @throws java.lang.Exception 
     */
    public MainDataListBean getAllMainDatas(MainDataMetaDataEntity mdmd) throws Exception{
        MainDataListBean mdl = new MainDataListBean();
        mdl.setMdmd(mdmd);
        List<MainDataContener> list = new ArrayList();
        mdl.setDatas(list);
        String sql = MainDataSQLBuilder.getSelectSQL(mdmd);
        Connection cn = C3p0Source.getConnection();
        
        Statement d = null;
        ResultSet rs = null;
        try {
             d = cn.createStatement();
             rs = d.executeQuery(sql);
             
             while(rs.next()){
                 MainDataContener mm = new MainDataContener();
                 List<MainDataPropertyEntity> ps = mdmd.getProperties();
                 for(MainDataPropertyEntity p : ps){
                     String name = p.getName();
                     Object o = rs.getObject(name);
                     mm.SetValue(name, o);
                 }
                 list.add(mm);
             }
        } catch (SQLException ex) {
            Logger.getLogger(MetaDataDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs,d,cn);
        }
        return mdl;
    }
    
    /**
     * 根据主数据描述获取所有主数据记录
     * @param mdmd
     * @param id
     * @return 
     * @throws java.lang.Exception 
     */
    public MainDataBean getMainDatas(MainDataMetaDataEntity mdmd,String id) throws Exception{
        MainDataBean mainDataBean = new MainDataBean();
        mainDataBean.setMdmd(mdmd);
        MainDataContener mc = new MainDataContener();
        
        String sql = MainDataSQLBuilder.getSelectSQL(mdmd);
        sql = sql + " where id=?";
        
        Connection cn = C3p0Source.getConnection();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
             ps = cn.prepareStatement(sql);
             ps.setString(1, id);
             rs = ps.executeQuery();
             
             if(rs.next()){
                 MainDataContener mm = new MainDataContener();
                 List<MainDataPropertyEntity> mainDataProperties= mdmd.getProperties();
                 for(MainDataPropertyEntity p : mainDataProperties){
                     String name = p.getName();
                     Object o = rs.getObject(name);
                     mc.SetValue(name, o);
                 }
                 mainDataBean.setMc(mc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MetaDataDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs,ps,cn);
        }
        return mainDataBean;
    }
    
    /**
     * 获取该主数据的某个副本数据的所有数据
     * @param mdmd
     * @param cname
     * @return 
     * @throws java.lang.Exception 
     */
    public MainDataListBean getAllCorbonDatas(MainDataMetaDataEntity mdmd,String cname) throws Exception{
        MainDataListBean mdl = new MainDataListBean();
        mdl.setMdmd(mdmd);
        List<MainDataContener> list = new ArrayList();
        mdl.setDatas(list);
        String sql = MainDataSQLBuilder.getCorbonSelectSQL(mdmd,cname);
        Connection cn = C3p0Source.getConnection();
        
        Statement d = null;
        ResultSet rs = null;
        try {
             d = cn.createStatement();
             rs = d.executeQuery(sql);
             
             while(rs.next()){
                 MainDataContener mm = new MainDataContener();
                 List<MainDataPropertyEntity> ps = mdmd.getProperties();
                 for(MainDataPropertyEntity p : ps){
                     String name = p.getName();
                     Object o = rs.getObject(name);
                     mm.SetValue(name, o);
                 }
                 list.add(mm);
             }
        } catch (SQLException ex) {
            Logger.getLogger(MetaDataDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs,d,cn);
        }
        return mdl;
    }
    
    /**
     * 增加一条主数据
     * @param mdb 
     * @throws java.lang.Exception 
     */
    public void add(MainDataBean mdb) throws Exception{
        
        MainDataMetaDataEntity mdmd = mdb.getMdmd();
        List<MainDataPropertyEntity> ps = mdmd.getProperties();
        MainDataContener mdc = mdb.getMc();
        String mainID = UUID.randomUUID().toString();
        mdc.SetValue("id", mainID);
        String sql = MainDataSQLBuilder.buildInsertSQL(mdmd);
        Connection cn = C3p0Source.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = cn.prepareStatement(sql);
            for(int i = 0;i < ps.size();i ++){
                MainDataPropertyEntity p = ps.get(i);
                String name = p.getName();
                switch (p.getDatatype()) {
                    case MainDataPropertyEntity.DT_STRING : {
                        preparedStatement.setString(i + 1, (String)mdc.getValue(name));
                        break;
                    }
                    case MainDataPropertyEntity.DT_INT : {
                        preparedStatement.setInt(i + 1, (Integer)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_DATE : {
                        preparedStatement.setDate(i + 1, (Date)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_DOUBLE : {
                        preparedStatement.setDouble(i + 1, (Double)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_FLOAT : {
                        preparedStatement.setFloat(i + 1, (Float)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_LONG : {
                        preparedStatement.setLong(i + 1, (Long)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_TIME : {
                        preparedStatement.setTime(i + 1, (Time)mdc.getValue(name));
                         break;
                    }
                    default:{
                         break;
                    }
                }
            }
            preparedStatement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(MainDataService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(null, preparedStatement, cn);
        }
                
    }
    
    public void updateMainData(MainDataBean mdb) throws Exception{
        
        MainDataMetaDataEntity mdmd = mdb.getMdmd();
        List<MainDataPropertyEntity> ps = mdmd.getProperties();
        MainDataContener mdc = mdb.getMc();
        String sql = MainDataSQLBuilder.buildUpdateSQL(mdmd, ps);
        Connection cn = C3p0Source.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = cn.prepareStatement(sql);
            for(int i = 0;i < ps.size();i ++){
                MainDataPropertyEntity p = ps.get(i);
                String name = p.getName();
                String type = p.getDatatype();
                switch (type) {
                   case MainDataPropertyEntity.DT_STRING : {
                        preparedStatement.setString(i + 1, (String)mdc.getValue(name));
                        break;
                    }
                    case MainDataPropertyEntity.DT_INT : {
                        preparedStatement.setInt(i + 1, (Integer)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_DATE : {
                        preparedStatement.setDate(i + 1, (Date)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_DOUBLE : {
                        preparedStatement.setDouble(i + 1, (Double)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_FLOAT : {
                        preparedStatement.setFloat(i + 1, (Float)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_LONG : {
                        preparedStatement.setLong(i + 1, (Long)mdc.getValue(name));
                         break;
                    }
                    case MainDataPropertyEntity.DT_TIME : {
                        preparedStatement.setTime(i + 1, (Time)mdc.getValue(name));
                         break;
                    }
                }
                
            }
            preparedStatement.setString(ps.size() + 1, (String)mdc.getValue("id"));
            
            preparedStatement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(MainDataService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(null, preparedStatement, cn);
        }
                
    }
    
    public void deleteMainData(MainDataBean mdb) throws Exception{
        
        String sql = "delete from " + MainDataSQLBuilder.getTableName(mdb.getMdmd()) + " where id=?";
        Connection cn = C3p0Source.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = cn.prepareStatement(sql);
            preparedStatement.setString(1, (String)mdb.getMc().getValue("id"));
            preparedStatement.execute();
        } catch (SQLException ex) {
            Logger.getLogger(MainDataService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(null, preparedStatement, cn);
        }
    }
}
