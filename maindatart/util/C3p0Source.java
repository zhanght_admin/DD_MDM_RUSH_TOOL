/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maindatart.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dingding
 */
public final class C3p0Source {

    private C3p0Source() {
    }

    private static ComboPooledDataSource dataSource;

    static {
        try {
            dataSource = new ComboPooledDataSource();
            Properties properties = new Properties();
            InputStream is = C3p0Source.class.getClassLoader().getResourceAsStream("jdbc.properties");
            properties.load(is);
            dataSource.setUser(properties.getProperty("user"));
            dataSource.setPassword(properties.getProperty("password"));
            dataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
            dataSource.setDriverClass(properties.getProperty("driverClass"));

            dataSource.setInitialPoolSize(Integer.parseInt(properties.getProperty("initialPoolSize")));
            dataSource.setMinPoolSize(Integer.parseInt(properties.getProperty("minPoolSize")));
            dataSource.setMaxPoolSize(Integer.parseInt(properties.getProperty("maxPoolSize")));
            dataSource.setMaxIdleTime(Integer.parseInt(properties.getProperty("maxIdleTime")));
            dataSource.setIdleConnectionTestPeriod(Integer.parseInt(properties.getProperty("idleConnectionTestPeriod")));
            dataSource.setMaxConnectionAge(Integer.parseInt(properties.getProperty("maxConnectionAge")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

     public static void realseSource(ResultSet rs, Statement st, Connection conn){
        try {
            if (rs != null) {
                rs.close();
            }
        } catch(SQLException e){}finally {
            try {
                if (st != null) {
                    st.close();
                }
            }catch(SQLException e){} finally {
                try{
                if (conn != null) {
                    conn.close();
                }}catch(SQLException e){}
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        Connection c = C3p0Source.getConnection();

        String sql = "select * from maindata_metadata";
        Statement d = null;
        ResultSet rs = null;
        try {
            d = c.createStatement();
            rs = d.executeQuery(sql);
            while (rs.next()) {
                String id = rs.getString("id");
                String name = rs.getString("name");
                String en_name = rs.getString("en_name");
                int count = rs.getInt("cobon");
                    
                System.out.println(id + "#" + name + "#" + en_name + "#" + count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(C3p0Source.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        } finally {
            C3p0Source.realseSource(rs, d, c);
        }

    }
}
