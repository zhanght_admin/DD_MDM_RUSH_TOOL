/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.view;

import java.awt.Color;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import maindatart.maindata.MainDataBean;
import maindatart.maindata.MainDataService;
import maindatart.metadata.MDMetaDataService;
import maindatart.metadata.MainDataMetaDataEntity;
import maindatart.metadata.MainDataPropertyEntity;
import maindatart.rush.result.PendingDataEntity;
import maindatart.rush.result.RushReportEntity;
import maindatart.rush.result.RushResultService;

/**
 *
 * @author Administrator
 */
public class ResultDellPanel extends javax.swing.JPanel {

    private RushResultService rrService = new RushResultService();
    private MDMetaDataService mdmdService = new MDMetaDataService();
    private MainDataService mds = new MainDataService();
    
    private MainDataBean frist = null;
    private MainDataBean second = null;
    private MainDataMetaDataEntity pmdmd = null;
    
    private int currentPosition = 0;
    
    /**
     * Creates new form ResultDellJPanel
     */
    public ResultDellPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jTextField2 = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "id", "type", "主数据（甲）", "主数据（乙）", "相似度", "备注"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                showMainData(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jTextField1.setToolTipText("");

        jButton1.setText("查询");
        jButton1.setToolTipText("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("清洗编号");
        jLabel1.setToolTipText("");

        jLabel3.setText("主数据名");
        jLabel3.setToolTipText("");

        jTextField2.setToolTipText("");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jButton6.setText("合并");
        jButton6.setToolTipText("");
        jButton6.setEnabled(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton2.setText("不合并");
        jButton2.setToolTipText("");
        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel6.setText("清洗时间");

        jLabel7.setText("清洗结果");

        jTextField4.setToolTipText("");

        jButton3.setText("读取");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("导出excel");
        jButton4.setToolTipText("");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(26, 26, 26)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton1)
                                        .addGap(30, 30, 30)
                                        .addComponent(jButton4))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel6)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 699, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(220, 220, 220)
                                .addComponent(jButton3)
                                .addGap(18, 18, 18)
                                .addComponent(jButton6)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2)))
                        .addGap(0, 68, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton6)
                    .addComponent(jButton2))
                .addGap(18, 18, 18))
        );
    }// </editor-fold>//GEN-END:initComponents

    /**
     * 将第一行保存，第二行删除
     * @param evt 
     */
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        
        DefaultTableModel tm = (DefaultTableModel)jTable3.getModel();
        
        Vector<Vector> vs = tm.getDataVector();
        if(vs.size() == 2){
            Vector v1 = vs.get(0);
        Vector v2 = vs.get(1);
        List<MainDataPropertyEntity> list = pmdmd.getProperties();
        for(int i = 0;i < list.size();i ++){
            MainDataPropertyEntity pe  = list.get(i);
            String name = pe.getName();
            frist.getMc().SetValue(name, v1.get(i+1));
        }
        String dellID = (String)v2.get(1);
        
        try {
            mds.updateMainData(frist);
            mds.deleteMainData(second);
            dellPendingData();
            cleanTable2();
        } catch (Exception ex) {
            Logger.getLogger(ResultDellPanel.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this.getParent(), "出现异常"+ex.getMessage());
        }
        }
        
        
        jButton6.setEnabled(false);
        jButton2.setEnabled(false);
        jButton3.setEnabled(true);
        
        
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * 将table1 中position位置的pendingData移出，并将对应的pendingData
     * @param position 
     */
    private void dellPendingData() throws Exception{
        DefaultTableModel tm = (DefaultTableModel)jTable1.getModel();
        Object o = tm.getValueAt(this.currentPosition,0);
        String id = (String)o;
        
        if(tm.getRowCount() > 0){
            tm.removeRow(this.currentPosition);
        jTable1.setModel(tm);
        
        rrService.dellPendingDatas(id);
        
        }
    }
    
    private void cleanTable2(){
        DefaultTableModel tm = (DefaultTableModel)jTable3.getModel();
        int count = tm.getRowCount();
        for(int i = 0;i < count;i ++){
            tm.removeRow(0);
        }
        jTable3.setModel(tm);
    }
    /**
     * 查询清洗记录，进行后续处理
     * @param evt 
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        String id = jTextField1.getText();
        if(id == null || id.equals("")){
             JOptionPane.showMessageDialog(this.getParent(), "请输入清洗记录编码");
        }
        try {
            List<PendingDataEntity> list = rrService.getPendingDatas(id);
            RushReportEntity rre = rrService.getRushReportEntity(id);
            jTextField2.setText(rre.getMainDataName());
            jTextField3.setText(rre.getTime());
            jTextField4.setText(rre.getId());
            
            DefaultTableModel tm = (DefaultTableModel)jTable1.getModel();
            tm.setRowCount(0);
            int i = 1;
            for(PendingDataEntity pde : list){
                Vector v = new Vector();
                v.add(pde.getId());
                if(null != pde.getType())
                    switch (pde.getType()) {
                    case PendingDataEntity.SANE:
                        v.add("相同");
                        break;
                    case PendingDataEntity.LIKE:
                        v.add("相似");
                        break;
                    default:
                        v.add("其他");
                        break;
                }
                v.add(pde.getMaindataID1());
                v.add(pde.getMaindataID2());
                v.add(pde.getCompareValue());
                v.add(pde.getInfo());
                tm.addRow(v);
            }
            
            jTable1.setModel(tm);
        } catch (Exception ex) {
            Logger.getLogger(ResultDellPanel.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this.getParent(), "出现异常"+ex.getMessage());
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * 点击待处理数据，在主数据table中显示甲方主数据和乙方主数据
     * @param evt 
     */
    private void showMainData(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showMainData
        int row = jTable1.getSelectedRow();
        this.currentPosition = row;
        showDetailData(row);
        
    }//GEN-LAST:event_showMainData

    
    
    private void showDetailData(int row){
            try {
            
            DefaultTableModel tm = (DefaultTableModel)jTable1.getModel();
            String type = (String)tm.getValueAt(row, 1);
            String maindata1 = (String)tm.getValueAt(row, 2);
            String maindata2 = (String)tm.getValueAt(row, 3);
            
            String mainDataName = jTextField2.getText();
            MainDataMetaDataEntity mdmd = mdmdService.getMainDataMetaDataByName(mainDataName);
            pmdmd = mdmd;
            MainDataBean md1 = mds.getMainDatas(mdmd,maindata1);
            MainDataBean md2 = mds.getMainDatas(mdmd,maindata2);
            frist = md1;
            second = md2;
            List<MainDataPropertyEntity> properties = mdmd.getProperties();

            DefaultTableModel tm2 = (DefaultTableModel)jTable3.getModel();
            tm2.setColumnCount(0);
            tm2.addColumn("方向");
            for(MainDataPropertyEntity p : properties){
                String pname = p.getName();
                tm2.addColumn(pname);
            }
            jTable3.repaint();
            
            Vector v1 = new Vector();
            v1.add("甲");
            Vector v2 = new Vector();
            v2.add("乙");
            for(MainDataPropertyEntity p : properties){
                String pname = p.getName();
                if(md1.getMc() != null)
                    v1.add(md1.getMc().getValue(pname));
                if(md2.getMc() != null)
                    v2.add(md2.getMc().getValue(pname));
            }
            tm2.setRowCount(0);
            tm2.addRow(v1);
            tm2.addRow(v2);
            
            jTable3.setModel(tm2);
           
        } catch (Exception ex) {
            Logger.getLogger(ResultDellPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * 读取表格中的一条数据，并将这条数据置灰
     * @param evt 
     */
    
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int row = jTable1.getRowCount();
        if(row > 0)
            showDetailData(0);
        else
            JOptionPane.showMessageDialog(this.getParent(), "已经没有数据");
        this.currentPosition = 0;
        jButton6.setEnabled(true);
        jButton2.setEnabled(true);
        jButton3.setEnabled(false);
    }//GEN-LAST:event_jButton3ActionPerformed

    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            dellPendingData();
            cleanTable2();
            jButton6.setEnabled(false);
            jButton2.setEnabled(false);
            jButton3.setEnabled(true);
        } catch (Exception ex) {
            Logger.getLogger(ResultDellPanel.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this.getParent(), "出现异常"+ex.getMessage());
        }
       
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        String rushRecordId = jTextField1.getText();
        if(rushRecordId == null || rushRecordId.equals("")){
            JOptionPane.showMessageDialog(this.getParent(), "请输入记录号");
            return;
        }
        
        String file = "c:/" + rushRecordId + ".xls";
        try {
            rrService.writeInExcelFile(file, rushRecordId);
            JOptionPane.showMessageDialog(this.getParent(), "导出成功!"+ file);
        } catch (Exception ex) {
            Logger.getLogger(ResultDellPanel.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this.getParent(), "出现异常"+ex.getMessage());
        }
        
    }//GEN-LAST:event_jButton4ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
