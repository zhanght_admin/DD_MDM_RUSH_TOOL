/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package maindatart.rush.alg;



/**
 *  数据一致性判定算法
 * 
 * @author 丁丁
 * @param <T>
 * @param <A>
 */
public interface DataSameAlgorithm <T,A>{
    
   
    
    /**
     * 判断两个数据项的相似度
     * 
     * 原则：
     *  在人工可接受范围内，尽量多的相似数据。
     * 
     * @param a
     * @param b
     * @return
     */
    public double itemSame(T a,A b);
    
    /**
     *  关于算法的描述
     * @return 
     */
    public String getDescrip();
}
