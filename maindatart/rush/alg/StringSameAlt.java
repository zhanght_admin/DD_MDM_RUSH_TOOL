/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.alg;

/**
 *
 * @author Administrator
 */
public class StringSameAlt implements DataSameAlgorithm {

    @Override
    public double itemSame(Object a, Object b) {
        String aa = (String)a;
        String bb = (String)b;
        
        if(aa.equals(bb))
            return 1;
        else
            return 0;
    }
    private static StringSameAlt s = null;
    
    private StringSameAlt(){
        
    }
    
    public static StringSameAlt getInstance(){
        if(s == null){
            s = new StringSameAlt();
        }
        return s;
    }
    
       @Override
    public String getDescrip() {
        return "字符完全匹配算法";
    }
}
