/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.alg;

/**
 *
 * @author dingding
 * 
 * 默认算法
 * 
 */
public class DefaultSameAlt implements DataSameAlgorithm {

    @Override
    public double itemSame(Object a, Object b) {
       
        if(a.equals(b))
            return 1;
        else
            return 0;
    }
    
    private static DefaultSameAlt s = null;
    
    private DefaultSameAlt(){
        
    }
    
    public static DefaultSameAlt getInstance(){
        if(s == null){
            s = new DefaultSameAlt();
        }
        return s;
    }

    @Override
    public String getDescrip() {
        return "默认算法";
    }
}
