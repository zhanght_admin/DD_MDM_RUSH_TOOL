/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.alg;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 丁丁
 * 
 * 算法的工厂类
 */
public class DataSameAlgFactory {
    
    /**
     * 较短的字符
     */
    public static final String ALT_STRING = "string";
    public static final String ALT_INTEGER = "int";
    public static final String ALT_TEXT_FENCI = "text_fenci";
//    public static final String ALT_TEXT_PINYIN = "text_PINYIN";
    public static final String ALT_TEXT_SAME = "text_same";
    
    public static final List<String> typeList = new ArrayList();
    static {
            typeList.add(ALT_STRING);
            typeList.add(ALT_INTEGER);
            typeList.add(ALT_TEXT_SAME);
            typeList.add(ALT_TEXT_FENCI);
        }    
        
    
    public static DataSameAlgorithm getDataSameAlgorithm(String type){
        switch (type) {
            case ALT_STRING:
                return StringSameAlt.getInstance();
            case ALT_INTEGER:
                return IntegerSameAlg.getInstance();
            case ALT_TEXT_SAME:
                return TextSameAlt.getInstance();
            case ALT_TEXT_FENCI:
                return TextFenciSameAlt.getInstance();
        }
        return DefaultSameAlt.getInstance();
    }
}
