/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.alg;

/**
 *
 * @author 整形数据一致性判定方法
 * 
 * 要求完全一样
 * 
 */
public class IntegerSameAlg implements DataSameAlgorithm {

    @Override
    public double itemSame(Object a, Object b) {
       int a1 = ((Integer)a);
       int b1 = ((Integer)b);
       if(a1 == b1)
           return  1;
       else
           return 0;
    }

    
     private static IntegerSameAlg s = null;
    
    private IntegerSameAlg(){
        
    }
    
    public static IntegerSameAlg getInstance(){
        if(s == null){
            s = new IntegerSameAlg();
        }
        return s;
    }
    
    @Override
    public String getDescrip() {
        return "整型数据的对比";
    }
}
