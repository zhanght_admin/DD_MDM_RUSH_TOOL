/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import maindatart.util.C3p0Source;

/**
 *
 * @author dingding
 * 
 * 对主数据配置刷新算法
 * 
 */
public class ConfigDao {
    
    //
    public MainDataRashConfig getMainDataRashConfigByID(String id) throws SQLException{
        String sql = "select id,maindataname,samevalue,differentvalue from rush_config where id=? ";
        Connection cn = C3p0Source.getConnection();
        MainDataRashConfig cf = null;
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, id);
            rs = d.executeQuery();
            if(rs.next()){
                cf = new MainDataRashConfig();
                cf.setId(id);
                cf.setMainDataName(rs.getString("maindataname"));
                cf.setSame(rs.getDouble("samevalue"));
                cf.setDifferent(rs.getDouble("differentvalue"));
                
                cf.setItemConfig(this.getPropertities(id));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
        
        return cf;
    }
    
    public List<MainDataRashConfig> getMainDataRashConfigByMainDataName(String maindataname) throws SQLException{
        String sql = "select id,maindataname,samevalue,differentvalue from rush_config where maindataname = ? ";
        Connection cn = C3p0Source.getConnection();
        List<MainDataRashConfig> mrcs = new ArrayList();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, maindataname);
            rs = d.executeQuery();
            while(rs.next()){
                MainDataRashConfig cf = new MainDataRashConfig();
                cf.setId(rs.getString("id"));
                cf.setMainDataName(rs.getString("maindataname"));
                cf.setSame(rs.getDouble("samevalue"));
                cf.setDifferent(rs.getDouble("differentvalue"));
                
                cf.setItemConfig(this.getPropertities(cf.getId()));
                mrcs.add(cf);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
        return mrcs;
    }
    
    private List<PropertityConfig> getPropertities(String configID) throws SQLException{
        List<PropertityConfig> pcf = new ArrayList();
        
        String sql = "select id,itemname,proporation,algname from rush_propertity_config where configid = ? ";
        Connection cn = C3p0Source.getConnection();
        MainDataRashConfig cf = null;
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, configID);
            rs = d.executeQuery();
            while(rs.next()){
                PropertityConfig pc = new PropertityConfig();
               
                pc.setId(rs.getString("id"));
                pc.setConfigId(configID);
                pc.setProportion(rs.getDouble("proporation"));
                pc.setAlgname(rs.getString("algname"));
                pc.setItmename(rs.getString("itemname"));
                pcf.add(pc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
        
        return pcf;
    }
    
    public void insert(MainDataRashConfig mdrc) throws SQLException{
        String sql = "insert into rush_config(id,maindataname,samevalue,differentvalue) values(?,?,?,?)";
        String mainID = UUID.randomUUID().toString();
        Connection cn = C3p0Source.getConnection();
        MainDataRashConfig cf = null;
        
        PreparedStatement ps = null;
         try {
            ps = cn.prepareStatement(sql);
            ps.setString(1, mainID);
            ps.setString(2, mdrc.getMainDataName());
            ps.setDouble(3, mdrc.getSame());
            ps.setDouble(4, mdrc.getDifferent());
            
            ps.execute();
            
            List<PropertityConfig> pcs = mdrc.getItemConfig();
            for(PropertityConfig pc : pcs){
                String sql1 = "insert into rush_propertity_config(id,configid,itemname,proporation,algname) values(?,?,?,?,?)";
                
                ps = cn.prepareStatement(sql1);
                ps.setString(1, UUID.randomUUID().toString());
                ps.setString(2, mainID);
                ps.setString(3, pc.getItmename());
                ps.setDouble(4, pc.getProportion());
                ps.setString(5, pc.getAlgname());
                
                ps.execute();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(null, ps, cn);
        }
    }
    
    
    
}
