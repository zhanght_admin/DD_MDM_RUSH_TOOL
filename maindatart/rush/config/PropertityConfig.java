/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.config;

/**
 *
 * @author dingding
 * 
 * 对每个属性的配置
 * 
 * 占的比例，使用的算法
 * 
 */
public class PropertityConfig {

    //数据库ID
    String id;
    //主表ID
    String configId;
    //所占比例
    double proportion;
    //使用算法
    String algname;
    //对应属性
    String itmename;

    public String getItmename() {
        return itmename;
    }

    public void setItmename(String itmename) {
        this.itmename = itmename;
    }

   
            
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public double getProportion() {
        return proportion;
    }

    public void setProportion(double proportion) {
        this.proportion = proportion;
    }

    public String getAlgname() {
        return algname;
    }

    public void setAlgname(String algname) {
        this.algname = algname;
    }
    
     //校验函数
    public boolean valedata(){
        return true;
    }
}
