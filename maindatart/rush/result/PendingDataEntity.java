package maindatart.rush.result;

/**
 * 
 * @author zhangxu
 * 
 * 记录清洗过程中产生的待处理数据
 *  如：1、数据判定相同，但是需要确定数据项内容的数据；
 *     2、数据判定相似，需要合并或者拆分的数据。
 *
 */
public class PendingDataEntity {

    public static final String SANE = "1";
    
    public static final String LIKE = "2";
    
    //未处理
    public static final String DELL_STATE = "1";
    //已处理
    public static final String NOT_DELL_STATE = "0";

    //
    private String id;
    //清洗记录ID
    private String rushID;
    //1 相同  2相似
    private String type;
    //甲方
    private String maindataID1;
    //乙方
    private String maindataID2;
    /**
     * 比较结果（相似度值）
     */
    private double compareValue;
    /**
     * 处理标志，默认为0，没有处理。处理完成后为1
     */
    private String dt = "0";

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    
    /**
     * 比较过程信息
     */
    private String info;
            
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRushID() {
        return rushID;
    }

    public void setRushID(String rushID) {
        this.rushID = rushID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaindataID1() {
        return maindataID1;
    }

    public void setMaindataID1(String maindataID1) {
        this.maindataID1 = maindataID1;
    }

    public String getMaindataID2() {
        return maindataID2;
    }

    public void setMaindataID2(String maindataID2) {
        this.maindataID2 = maindataID2;
    }
    public double getCompareValue() {
        return compareValue;
    }

    public void setCompareValue(double compareValue) {
        this.compareValue = compareValue;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
