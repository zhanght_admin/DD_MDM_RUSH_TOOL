/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package maindatart.rush.result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import maindatart.rush.config.ConfigDao;
import maindatart.rush.config.MainDataRashConfig;
import maindatart.util.C3p0Source;

/**
 *
 * @author Administrator
 */
public class RushResultDao {
    
    /**
     * 保存RushReportEntity
     * @param rushReport 
     */
    public void insertRushResult(RushReportEntity rushReport) throws SQLException{
        String sql = "insert into rush_report(id,info,maindataname,algname,time) values(?,?,?,?,?)";
        Connection cn = C3p0Source.getConnection();
        List<MainDataRashConfig> mrcs = new ArrayList();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            String mainID = UUID.randomUUID().toString();
            if(rushReport.getId() == null || rushReport.getId().equals(""))
                d.setString(1, mainID);
            else
                d.setString(1, rushReport.getId());
            d.setString(2, rushReport.getInfo());
            d.setString(3, rushReport.getMainDataName());
            d.setString(4, rushReport.getAlgName());
            d.setString(5, rushReport.getTime());
            d.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    
    public void insertRushPendingData(PendingDataEntity pendingData)throws SQLException{
        String sql = "insert into rush_pendingdata (id,rushid,type,maindata1,maindata2,info,comparevalue,dt) values(?,?,?,?,?,?,?,?)";
        Connection cn = C3p0Source.getConnection();
        List<MainDataRashConfig> mrcs = new ArrayList();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            String mainID = UUID.randomUUID().toString();
            d.setString(1, mainID);
            d.setString(2, pendingData.getRushID());
            d.setString(3, pendingData.getType());
            d.setString(4, pendingData.getMaindataID1());
            d.setString(5, pendingData.getMaindataID2());
            d.setString(6, pendingData.getInfo());
            d.setDouble(7, pendingData.getCompareValue());
            d.setString(8, pendingData.getDt());
            d.execute();
            
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    
    public List<RushReportEntity> getRushReportEntitylist(String mainDataName)throws Exception{
        List<RushReportEntity> list = new ArrayList();
        
        String sql = "select id,info,algname,time  from rush_report where maindataname=?";
        Connection cn = C3p0Source.getConnection();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, mainDataName);
            rs = d.executeQuery();
            while(rs.next()){
                RushReportEntity rre = new RushReportEntity();
                String id = rs.getString("id");
                String info = rs.getString("info");
                String algname = rs.getString("algname");
                String time = rs.getString("time");
                rre.setId(id);
                rre.setInfo(info);
                rre.setMainDataName(mainDataName);
                rre.setTime(time);
                list.add(rre);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    
    public RushReportEntity getByID(String id) throws SQLException{
        RushReportEntity rushReportEntity = new RushReportEntity();
        rushReportEntity.setId(id);
        String sql = "select id,info,algname,time,mainDataName  from rush_report where id=? ";
        Connection cn = C3p0Source.getConnection();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, id);
            rs = d.executeQuery();
            if(rs.next()){
                String info = rs.getString("info");
                String algname = rs.getString("algname");
                String time = rs.getString("time");
                String mainDataName = rs.getString("mainDataName");
                
                rushReportEntity.setId(id);
                rushReportEntity.setInfo(info);
                rushReportEntity.setMainDataName(mainDataName);
                rushReportEntity.setTime(time);
            }
            return rushReportEntity;
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    /**
     * 按照逆序
     * @param forgenKey
     * @return
     * @throws Exception 
     */
    public List<PendingDataEntity> getPendingDatasDesc(String forgenKey,String state)throws Exception{
        List<PendingDataEntity> list = new ArrayList();
        
        String sql = "select id,type,maindata1,maindata2,info,comparevalue  from rush_pendingdata where rushid=? and dt=?";
        Connection cn = C3p0Source.getConnection();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, forgenKey);
            d.setString(2, state);
            rs = d.executeQuery();
            while(rs.next()){
                PendingDataEntity pde = new PendingDataEntity();
                String id = rs.getString("id");
                String type = rs.getString("type");
                String maindata1 = rs.getString("maindata1");
                String maindata2 = rs.getString("maindata2");
                pde.setId(id);
                pde.setRushID(forgenKey);
                pde.setType(type);
                pde.setMaindataID1(maindata1);
                pde.setMaindataID2(maindata2);
                pde.setInfo(rs.getString("info"));
                pde.setCompareValue(rs.getDouble("comparevalue"));
                list.add(0,pde);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    
    public void upDatePendingDataDt(String id,String dt)throws SQLException{
        String sql = "update rush_pendingdata set dt=? where id=?";
        Connection cn = C3p0Source.getConnection();
        
        PreparedStatement d = null;
        ResultSet rs = null;
        
        try {
            d = cn.prepareStatement(sql);
            d.setString(1, dt);
            d.setString(2, id);
            d.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConfigDao.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }finally{
            C3p0Source.realseSource(rs, d, cn);
        }
    }
    
//    
//    
//    public static void main(String[] args){
//        List<String> list = new ArrayList();
//        list.add(0, "a");
//        list.add(0, "b");
//        
//        for(String s : list){
//            System.out.println(s);
//        }
//    }
}
